// Lesson 3
function fizzBuzz () {
    const result = []
    for (let i = 1; i <= 100; i++) {
        if(i % 15 == 0){
            result.push("FizzBuzz")
        }else if(i % 3 == 0){
            result.push("Fizz")

        }else if(i % 5 == 0){
            result.push("Buzz")

        }else{
            result.push(i)
        }
    }
    return result
}

function filterArray (arr) {
    let result = []
    const arrays = arr.filter(item => Array.isArray(item)).forEach(element => {
        result = result.concat(element)
    });
    return [... new Set(result)]
}


// Output DON'T TOUCH!
console.log(fizzBuzz())
console.log(filterArray([ [2], 23, 'dance', true, [3, 5, 3] ]))